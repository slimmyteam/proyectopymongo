import pymongo
from pymongo import MongoClient
from datetime import datetime
from pprint import pprint
from MongoEngineClase import migration, book, calculate

# Conectar-se a DB

uri = "mongodb+srv://agomezl2:pyZBqY7j8Spummwt@airbnb.botpx85.mongodb.net/"

client = MongoClient(uri)
pprint(client.list_database_names())

db = client['sample_airbnb']
print("La BD es:")
pprint(db) # Imprimeix: 1) els 3 nodes de servidor que te el cluster (conjunt de servidors interconnectats que treballen junts per emmagatzemar i administrar dades); 2) la base de dades
coleccion = db['listingsAndReviews']
print("Longitud: " + str(coleccion.count_documents({})))

# MENU DE COMANDES

while (True):
    lineaentrada = input("Introdueix la comanda: ").split(" ")
    print("Has introduït: " + lineaentrada[0].lower())

    if lineaentrada[0].lower() == "fi":  # Per sortir del menu
        break

    elif lineaentrada[0].lower() == "google":  # PART 1 - CONSULTES

        columna = lineaentrada[2]
        consulta = lineaentrada[1]
        print("Consulta: " + consulta)

        # find_one (filter): de la coleccio, busquem un document que tingui el id que hem introduït a la comanda;
        # (args): 0 pels camps que no volem que es mostrin, i 1 pels que sí
        docGoogle = coleccion.find_one({"_id": columna}, {"_id": 0, "name": 1, consulta: 1})
        if consulta in docGoogle: # Comprovem si docGoogle te el camp 'consulta'
            tuplaGoogle = (docGoogle["name"], docGoogle[consulta]) # Ens guardem en una tupla el camp 'name' i el camp 'consulta'
            if 'cleaning_fee' in docGoogle and consulta == "cleaning_fee": # Si la consulta es de cleaning_fee, imprimim el valor
                valor = docGoogle["cleaning_fee"]
                print(valor)
            else:
                pprint(tuplaGoogle) # Si la consulta no es de cleaning_fee, imprimim la tupla
        else:
            print("No té aquest camp " + consulta) # Si el document no te el camp consultat, ens mostrara aquest missatge

    elif lineaentrada[0].lower() == "cancel":  # PART 2 - DELETE
        ide = lineaentrada[1]
        listaAbansBorrar = db['booking'] # Agafem els objectes de la db 'booking'
        listaAbansBorrarNum = str(listaAbansBorrar.count_documents({})) # Comptem el num. de documents
        print("Documents abans de borrar: " + listaAbansBorrarNum)
        listaAbansBorrar.delete_many({'id_place': ide}) # Eliminem tots els documents que continguin l'id que hem posat a la comanda
        listaDespresBorrar = db['booking'] # Tornem a agafar els objectes de la db 'booking' una vegada els hem esborrat
        listaDespresBorrarNum = str(listaDespresBorrar.count_documents({})) # Tornem a mostrar el num. de documents que queden despres d'esborrar
        print("Documents després de borrar: " + listaDespresBorrarNum)
        for b00k in listaDespresBorrar.find({}): # Amb el find, agafem tots els documents (ja que no posem filtres)
            print(b00k.get('name'))
        print("Final bucle")

    elif lineaentrada[0].lower() == "migration":
        migration()

    elif lineaentrada[0].lower() == "book":
        book()

    elif lineaentrada[0].lower() == "calculate":
        calculate()

    else:
        print("T'has equivocat")
