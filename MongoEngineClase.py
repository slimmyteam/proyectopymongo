import random
from decimal import Decimal

from mongoengine import *
from pprint import pprint

uri = "mongodb+srv://agomezl2:pyZBqY7j8Spummwt@airbnb.botpx85.mongodb.net/"

client = connect(host=uri, db='sample_airbnb')
print(client)

# PART 1 - MAPEIG I RESTRICCIONS

AMENITIES = (
    "Private pool", "BBQ grill", "Wheelchair accessible", "Window guards", "Stair gates", "Standing valet", "Shampoo",
    "Dog(s)", "Self check-in", "Sonos sound system", "Pets live on this property", "Hot tub", "Smoke detector",
    "Paid parking off premises", "Hot water", "Hangers", "Wide entryway", "Bed linens", "Smart lock",
    "Bedroom comforts",
    "Bathtub with bath chair", "Private living room", "Cat(s)", "Hair dryer", "EV charger", "Baby monitor", "Microwave",
    "Fax machine", "Bicycle", "Private hot tub", "Shared pool", "Wide hallway clearance", "Dining area", "Pets allowed",
    "TV", "Coffee maker", "Snorkeling equipment", "Table corner guards", "Gas oven", "Private bathroom", "Day bed",
    "Beachfront", "Ground floor access", "Swimming pool", "Chef's kitchen", "Pack ’n Play/travel crib", "Murphy bed",
    "Toilet paper", "Refrigerator", "Elevator", "Children’s dinnerware", "Memory foam mattress", "Cooking basics",
    "Essentials", "Lake access", "DVD player", "Heating", "Lock on bedroom door", "Host greets you", "Beach essentials",
    "Well-lit path to entrance", "Breakfast table", "Firm mattress", "Central air conditioning", "En suite bathroom",
    "Beach view", "Safety card", "Buzzer/wireless intercom", "Pool with pool hoist", "Smart TV",
    "Disabled parking spot",
    "Air purifier", "Kitchen", "Step-free access", "Internet", "Stove", "Family/kid friendly", "Bathtub",
    "Fireplace guards", "Balcony", "Convection oven", "Building staff", "Bath towel", "Fixed grab bars for shower",
    "Safe",
    "Full kitchen", "Room-darkening shades", "Toaster", "Paid parking on premises", "Handheld shower head",
    "Tennis court",
    "24-hour check-in", "Crib", "Boogie boards", "Patio or balcony", "Breakfast bar", "Parking",
    "Long term stays allowed",
    "Private entrance", "Other pet(s)", "Indoor fireplace", "Alfresco shower", "Wifi", "Pocket wifi",
    "translation missing: en.hosting_amenity_49", "Wide doorway", "Walk-in shower", "Pillow-top mattress",
    "First aid kit",
    "Doorman", "Wide clearance to shower", "Fixed grab bars for toilet", "Dishwasher", "Ceiling fan",
    "Heated towel rack",
    "Dishes and silverware", "Wide clearance to bed", "Rain shower", "Suitable for events", "Hot water kettle",
    "Permit parking", "Cable TV", "Washer", "High chair", "Double oven", "Mini fridge", "Ski-in/Ski-out", "Lockbox",
    "Washer / Dryer", "Bidet", "Gym", "toilet", "Sauna", "Luggage dropoff allowed", "Sound system", "Outdoor seating",
    "Ironing Board", "Oven", "Garden or backyard", "Ethernet connection", "Dryer", "Other", "Outdoor parking", "Iron",
    "Flat path to front door", "Pool", "Free street parking", "Keypad", "Bathroom essentials", "Roll-in shower",
    "Warming drawer", "Kayak", "Single level home", "Outlet covers", "Ice Machine", "Netflix", "Home theater",
    "Terrace",
    "Babysitter recommendations", "Waterfront", "Baby bath", "Mountain view", "Free parking on premises", "Breakfast",
    "Espresso machine", "Extra pillows and blankets", "Game console", "Accessible-height bed",
    "Children’s books and toys",
    "Kitchenette", "Electric profiling bed", "Changing table", "Air conditioning",
    "translation missing: en.hosting_amenity_50", "Body soap", "Accessible-height toilet", "Beach chairs",
    "Laptop friendly workspace", "Shower chair", "Smoking allowed", "Carbon monoxide detector", "Fire extinguisher",
    "Sun loungers", "Formal dining area", "Cleaning before checkout")
PROPERTY_TYPE = (
    "Loft", "Boutique hotel", "Cottage", "Treehouse", "Castle", "Tiny house", "Train", "Serviced apartment", "Hut",
    "Guest suite", "House", "Boat", "Guesthouse", "Farm stay", "Townhouse", "Pension (South Korea)", "Condominium",
    "Chalet", "Aparthotel", "Hotel", "Cabin", "Hostel", "Casa particular (Cuba)", "Villa", "Apartment", "Other",
    "Resort",
    "Heritage hotel (India)", "Nature lodge", "Bed and breakfast", "Earth house", "Barn", "Bungalow", "Camper/RV",
    "Houseboat", "Campsite")
ROOM_TYPE = ("Entire home/apt", "Shared room", "Private room")
BED_TYPE = ("Couch", "Real Bed", "Pull-out Sofa", "Airbed", "Futon")
SEASON = ('High', 'Mid', 'Low')


class Imatges(EmbeddedDocument):
    thumbnail_url = StringField()
    medium_url = StringField()
    picture_url = StringField()
    xl_picture_url = StringField()


class Host(EmbeddedDocument):
    host_id = StringField()
    host_url = StringField()
    host_name = StringField()
    host_location = StringField()
    host_about = StringField()
    host_response_time = StringField()
    host_thumbnail_url = StringField()
    host_picture_url = StringField()
    host_neighbourhood = StringField()
    host_response_rate = IntField()
    host_is_superhost = BooleanField()
    host_has_profile_pic = BooleanField()
    host_identity_verified = BooleanField()
    host_listings_count = IntField()
    host_total_listings_count = IntField()
    host_verifications = ListField(StringField())


class Location(EmbeddedDocument):
    type = StringField()
    coordinates = ListField(DecimalField())
    is_location_exact = BooleanField()


class Address(EmbeddedDocument):
    street = StringField()
    suburb = StringField()
    government_area = StringField()
    market = StringField()
    country = StringField()
    country_code = StringField()
    location = EmbeddedDocumentField(Location)


class Availability(EmbeddedDocument):
    availability_30 = IntField()
    availability_60 = IntField()
    availability_90 = IntField()
    availability_365 = IntField()


class Review_Scores(EmbeddedDocument):
    review_scores_accuracy = IntField()
    review_scores_cleanliness = IntField()
    review_scores_checkin = IntField()
    review_scores_communication = IntField()
    review_scores_location = IntField()
    review_scores_value = IntField()
    review_scores_rating = IntField()


class Review(EmbeddedDocument):
    _id = LongField()
    date = DateTimeField()
    listing_id = StringField()
    reviewer_id = StringField()
    reviewer_name = StringField()
    comments = StringField()


class ListingsAndReviews(Document):
    listing_url = StringField(required=True)
    name = StringField(required=True)
    summary = StringField()
    space = StringField()
    description = StringField()
    neighborhood_overview = StringField()
    notes = StringField()
    transit = StringField()
    access = StringField()
    interaction = StringField()
    house_rules = StringField()
    property_type = StringField(choices=PROPERTY_TYPE)
    room_type = StringField(choices=ROOM_TYPE)
    bed_type = StringField(choices=BED_TYPE)
    minimum_nights = IntField(min_value=1)
    maximum_nights = IntField(min_value=1)
    cancellation_policy = StringField()
    last_scraped = DateTimeField()
    calendar_last_scraped = DateTimeField()
    first_review = DateTimeField()
    last_review = DateTimeField()
    accommodates = IntField()
    bedrooms = IntField()
    beds = IntField()
    number_of_reviews = IntField()
    reviews_per_month = IntField()
    bathrooms = DecimalField(precision=2)
    amenities = StringField(choices=AMENITIES)
    price = DecimalField()
    weekly_price = DecimalField(precision=2)
    monthly_price = DecimalField(precision=2)
    cleaning_fee = DecimalField(precision=2)
    security_deposit = DecimalField(precision=2)
    extra_people = DecimalField(precision=2)
    guests_included = IntField()
    images = EmbeddedDocumentField(Imatges)
    host = EmbeddedDocumentField(Host)
    address = EmbeddedDocumentField(Address)
    availability = EmbeddedDocumentField(Availability)
    review_scores = EmbeddedDocumentField(Review_Scores)
    reviews = EmbeddedDocumentListField(Review)
    meta = {'collection': 'listingsAndReviews'}


class Customer(EmbeddedDocument):
    email = StringField()
    price_to_pay = DecimalField(precision=2, min_value=1)
    season = StringField(choices=SEASON)
    number_nights = IntField(max_value=30)


class Booking(Document):
    id_place = StringField(required=True)
    listing_url = StringField(required=True)
    name = StringField()
    address = EmbeddedDocumentField(Address)
    property_type = StringField(choices=PROPERTY_TYPE)
    price = DecimalField(precision=2)
    security_deposit = DecimalField()
    customers = EmbeddedDocumentListField(Customer)


####### Insert de prova (inici)

listing1 = ListingsAndReviews(listing_url="url", name="SitioRandom", beds=4, cleaning_fee=10.0)
listing2 = ListingsAndReviews(listing_url="url2", name="SitioRandom2", beds=2)
listing1.save()
listing2.save()

data1 = ListingsAndReviews.objects(name="SitioRandom")
data2 = ListingsAndReviews.objects(name="SitioRandom2")

# for i in data1:
#   print(i.name)
# for i in data2:
#   print(i.name)

data1.delete()
data2.delete()

####### Insert de prova (fi)


# PART 2 - MIGRATION

def migration():
    listaSecurityDepositTrue = ListingsAndReviews.objects(security_deposit__exists=True) # Recuperem els objects que tinguin el camp 'security_deposit'
    listaSecurityDepositFalse = ListingsAndReviews.objects(security_deposit__exists=False) # Recuperem els objects que NO tinguin el camp 'security_deposit'

    listaSecurityTrue = []
    listaSecurityFalse = []

    while len(listaSecurityTrue) < 5:
        listaSecurityTrue.append(listaSecurityDepositTrue[random.randint(0, len(listaSecurityDepositTrue))]) # Afegim aletoriament a la llista 5 documents que tenen el camp 'security_deposit'

    while len(listaSecurityFalse) < 5:
        listaSecurityFalse.append(listaSecurityDepositTrue[random.randint(0, len(listaSecurityDepositFalse))]) # Afegim aletoriament a la llista 5 documents que NO tenen el camp 'security_deposit'

    for data in listaSecurityTrue: # Per cada document en la llista, insertem (fem una copia) de cada document en la col·leccio Booking (que es crea automaticament quan insertem registres). En aquest cas, inicialitzem el camp 'security_deposits'
        booking1 = Booking(id_place=str(data.id), listing_url=data['listing_url'], name=data['name'],
                           address=data['address'],
                           property_type=data['property_type'], price=data['price'],
                           security_deposit=data['security_deposit'])
        booking1.save() # Insertem el booking creat

    for data in listaSecurityFalse: # Per cada document en la llista, insertem (fem una copia) de cada document en la col·leccio Booking (que es crea automaticament quan insertem registres). NO inicialitzem el camp 'security_deposits'
        booking2 = Booking(id_place=str(data.id), listing_url=data['listing_url'], name=data['name'],
                           address=data['address'],
                           property_type=data['property_type'], price=data['price'])
        booking2.save() # Insertem el booking creat


# PART 3 - BOOKING

def book():
    booking = Booking.objects() # Recuperem els objectes de la col·leccio booking

    booking_list = list(booking) # Convertir l'objecte de consulta a una llista de reserves (bookings)
    listaBookings = []

    if len(booking_list) >= 3: # Verifiquem si hi ha almenys 3 reserves
        listaBookings = random.sample(booking_list, 3) # Seleccionar 3 elements aleatoris
    else:
        print("No hay suficientes reservas en la colección.") # Imprimir en el cas que no hi hagi 3 reserves en la col·leccio bookings

    for data in listaBookings: # Dels 3 elements aleatoris seleccionats
        fieldcustomers = data.customers # Agafem el camp 'customers'
        email = "Usuario" + str(random.randint(0, 100)) + "@email.com" # Generem un e-mail aleatori (important posar 'str' al randomint)
        season = SEASON[random.randint(0, len(SEASON)-1)] # Agafem un SEASON aleatori
        number_nights = random.randint(1, 30) # Posem un numero de nits aleatori (min. 1 i max. 30)
        customer = Customer(email=email, season=season, number_nights=number_nights) # Creem un customer amb els valors anteriors
        fieldcustomers.append(customer) # Posem el customer creat en el camp 'customer' del booking
        data.customers = fieldcustomers # Posem el customer creat en el camp 'customer' del booking
        data.save() # Fem un .save del booking amb el customer dins ja actualitzat
        print(data.name)
    print("Reserva finalitzada")

# PART 4 - CALCULATE

def calculate():
    booking = Booking.objects() # Recuperem tots els objectes de la col·leccio booking
    booking_list = list(booking) # Fem una llista amb els objectes
    thebook = random.choice(booking_list) # Recuperem aleatoriament un objecte
    print("La booking escogida es: "+thebook.name)
    if thebook.customers == []:
        print("No tiene customers")
    else:
        print("Tiene customers")
        theCustomer = thebook.customers[random.randint(0, len(thebook.customers)-1)] # Si te customers, agafem un aleatoriament
        if theCustomer['price_to_pay'] != None: # Si te el camp 'price_to_pay
            print("El customer tiene el atributo price_to_pay")
        else:
            print("No tiene el atributo price_to_pay") # Si NO te el camp 'price_to_pay
            preu = thebook.price * theCustomer.number_nights # Multipliquem preu * num. de nits
            if thebook['security_deposit'] != None: # Si te el camp 'security_deposit'
                preu += thebook.security_deposit # Afegim el valor del 'security_deposit' al preu
            if theCustomer.season == "High":
                preu *= Decimal('1.25') # Si la SEASON es high: Multipliquem preu * 1.25
            elif theCustomer.season == "Low":
                preu *= Decimal('0.9') # Si la SEASON es low: Multipliquem preu * 0.9
            theCustomer.price_to_pay = preu # Assignem el valor del preu al camp 'price_to_pay'
            thebook.save() # Guardem el booking

