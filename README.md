# 📑 Aplicación de reservas Airbnb 📑

Aplicación en Python que realiza operaciones CRUD sobre una base de datos documental en Mongo.  

## 📄 Descripción
Proyecto realizado para la asignatura "Acceso a datos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻 Tecnologías
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)
- PyMongo.
- MongoEngine.
- MongoDB Atlas.
- MongoDB Compass.

## ⚙️ Funcionamiento
El funcionamiento se puede ver en el documento de pruebas del proyecto adjuntado a continuación: [Documento de pruebas](https://gitlab.com/slimmyteam/python/proyectopymongo/-/blob/main/Document_de_proves.pdf).

## ©️ Desarrolladores
- Ethan Corchero Martín.
- Ana María Gómez León.
- Selene Milanés Rodríguez.